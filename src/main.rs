use std::io;

fn main() {
    // This value is used for storing the current and previous values
    let mut number: (u32, u32) = (0, 1);
    // This value is used to track how many times the program has looped
    let mut i: u8 = 0;
    // This value is what number the program will generate
    let input: u8 = get_input();

    loop {
        number = generate_number(number.0, number.1);
        i = i + 1;

        if i == input {
            print_number(input, number.1);
            return;
        }
    }
}

fn get_input() -> u8 {
    loop {
        println!("Enter a number:");

        // The input provides a string, and has to be converted to u8 later on
        let mut input = String::new();

        io::stdin()
            .read_line(&mut input)
            .expect("failed");
    
        // Shadowing the String version of the input with a u8 version
        // u8 is used as this number is unable to go past 48
        let input: u8 = match input
            .trim()
            .parse() {
                Ok(num) => num,
                Err(_) => {
                    println!("Must be a number below 48!");
                    continue;
                },
            };

        // If the number is 48 or higher, the program crashes, as the number gets too big for u32
        if input >= 48 {
            println!("Must be a number below 48!");
            continue;
        }

        return input;
    }
}

// The math part
fn generate_number(current: u32, previous: u32) -> (u32, u32) {
    let i: u32 = current;
    let current: u32 = current + previous;
    let previous: u32 = i;
    return (current, previous);
}

// Ensuring that 1st, second, and 3rd all are handled correctly
// Why does English do this
fn print_number(input: u8, fibonacci: u32) {
    match input {
        1 => { println!("The {}st Fibonacci number is: {}", input, fibonacci) },
        2 => { println!("The {}nd Fibonacci number is: {}", input, fibonacci) },
        3 => { println!("The {}rd Fibonacci number is: {}", input, fibonacci) },
        _ => { println!("The {}th Fibonacci number is: {}", input, fibonacci) },
    }
}
